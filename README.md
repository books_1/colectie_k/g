# G

## Content

```
./G. B. Adamov:
G. B. Adamov - Taina celor doua oceane 2.0 '{SF}.docx

./G. Brigand:
G. Brigand - Gainat de Inger - Poeme inedite 0.99 '{Versuri}.docx

./G. D. Roberts:
G. D. Roberts - Umbra muntelui 1.0 '{Literatura}.docx

./G. E. Lessing:
G. E. Lessing - Natan inteleptul 0.9 '{Literatura}.docx

./G. H. Williamson:
G. H. Williamson - Lacasurile secrete ale leului 0.9 '{Spiritualitate}.docx

./G. K. Chesterton:
G. K. Chesterton - Hanul zburator 0.9 '{Literatura}.docx
G. K. Chesterton - Ortodoxia 0.99 '{Spiritualitate}.docx

./G. M. Ford:
G. M. Ford - Noapte fara nume 1.0 '{Literatura}.docx

./G. M. Vladescu:
G. M. Vladescu - Menuetul 0.6 '{Literatura}.docx

./G. P. Danilevski:
G. P. Danilevski - Mirovici. Printesa Tarakanova 1.0 '{Dragoste}.docx

./G. Soulie de Morant:
G. Soulie de Morant - Comoara samurailor 0.99 '{Tineret}.docx

./G. V. Zimin:
G. V. Zimin - Piloti de vanatoare V1 1.0 '{Razboi}.docx
G. V. Zimin - Piloti de vanatoare V2 1.0 '{Razboi}.docx

./G. Willow Wilson:
G. Willow Wilson - Alif Nevazutul 1.0 '{Supranatural}.docx

./Gabriela Adamesteanu:
Gabriela Adamesteanu - Drumul egal al fiecarei zile 1.0 '{Literatura}.docx

./Gabriela Melinescu:
Gabriela Melinescu - Ghetele fericirii 0.9 '{Nuvele}.docx

./Gabriel Andreescu:
Gabriel Andreescu - Polemici neortodoxe 0.99 '{Politica}.docx

./Gabriel Badea Paun:
Gabriel Badea Paun - Carmen Sylva 0.8 '{Politica}.docx

./Gabriel Cocu:
Gabriel Cocu - Dresajul cainelui de companie 0.9 '{Diverse}.docx

./Gabriel Dragan:
Gabriel Dragan - Pe frontul Marasesti invie mortii 1.0 '{Razboi}.docx

./Gabriel Garcia Marquez:
Gabriel Garcia Marquez - A trai pentru a-ti povesti viata 0.8 '{Dragoste}.docx
Gabriel Garcia Marquez - Aventura lui Miguel Littin 1.0 '{Dragoste}.docx
Gabriel Garcia Marquez - Blancaman cel bun, vanzator de miracole 1.0 '{Dragoste}.docx
Gabriel Garcia Marquez - Ceas rau 1.0 '{Dragoste}.docx
Gabriel Garcia Marquez - Colonelului n-are cine sa-i scrie 1.0 '{Dragoste}.docx
Gabriel Garcia Marquez - Cronica unei morti anuntate 1.0 '{Dragoste}.docx
Gabriel Garcia Marquez - Despre dragoste si alti demoni 1.1 '{Dragoste}.docx
Gabriel Garcia Marquez - Douasprezece povestiri calatoare 1.0 '{Dragoste}.docx
Gabriel Garcia Marquez - Dragostea in vremea holerei 1.0 '{Dragoste}.docx
Gabriel Garcia Marquez - Funeraliile mamei mari 1.0 '{Dragoste}.docx
Gabriel Garcia Marquez - Generalul in labirintul sau 1.0 '{Dragoste}.docx
Gabriel Garcia Marquez - Incredibila si trista poveste a candidei Erendira si a bunicii sale fara suflet 1.0 '{Dragoste}.docx
Gabriel Garcia Marquez - Inecatul cel mai frumos de pe lume 1.0 '{Dragoste}.docx
Gabriel Garcia Marquez - Marea timpului pierdut 1.0 '{Dragoste}.docx
Gabriel Garcia Marquez - Moarte constanta dincolo de dragoste 1.0 '{Dragoste}.docx
Gabriel Garcia Marquez - Ochi de caine albastru 1.0 '{Dragoste}.docx
Gabriel Garcia Marquez - Povestea tarfelor mele triste 1.0 '{Dragoste}.docx
Gabriel Garcia Marquez - Santiago Nasar 0.9 '{Dragoste}.docx
Gabriel Garcia Marquez - Stiri despre o rapire 0.6 '{Dragoste}.docx
Gabriel Garcia Marquez - Toamna patriarhului 1.1 '{Dragoste}.docx
Gabriel Garcia Marquez - Ultima calatorie a vasului fantoma 1.0 '{Dragoste}.docx
Gabriel Garcia Marquez - Un domn foarte batran cu niste aripi uriase 1.0 '{Dragoste}.docx
Gabriel Garcia Marquez - Un geniu i-si ia ramas bun 0.2 '{Dragoste}.docx
Gabriel Garcia Marquez - Un veac de singuratate 1.0 '{Dragoste}.docx
Gabriel Garcia Marquez - Vijelia 0.99 '{Dragoste}.docx

./Gabriel Iordan Dorobantu:
Gabriel Iordan Dorobantu - Duhovnicul 0.9 '{Diverse}.docx

./Gabriel Iuga:
Gabriel Iuga - Comoara regilor daci 1.0 '{ClubulTemerarilor}.docx

./Gabrielle Darcy:
Gabrielle Darcy - Iubire in vestul salbatic 0.99 '{Dragoste}.docx

./Gabriel Liiceanu:
Gabriel Liiceanu - Despre limita 0.99 '{Filozofie}.docx
Gabriel Liiceanu - Despre seductie 0.7 '{Filozofie}.docx
Gabriel Liiceanu - Intalnire cu un necunoscut 0.9 '{Filozofie}.docx
Gabriel Liiceanu - Jurnalul de la Paltinis 0.9 '{Filozofie}.docx
Gabriel Liiceanu - Usa interzisa 0.99 '{Filozofie}.docx

./Gabriel Osmonde:
Gabriel Osmonde - Calatoria unei femei care nu se mai temea de imbatranire 0.99 '{Literatura}.docx
Gabriel Osmonde - Lucrarea iubirii 1.0 '{Literatura}.docx

./Gabriel Tallent:
Gabriel Tallent - Iubirea mea desavarsita 1.0 '{Literatura}.docx

./Gae Polisner:
Gae Polisner - Privind la stele 1.0 '{Literatura}.docx

./Gailanne Maravel:
Gailanne Maravel - Am incredere in tine 0.9 '{Dragoste}.docx

./Gail Douglas:
Gail Douglas - Vreme furtunoasa 0.9 '{Dragoste}.docx

./Gail Honeyman:
Gail Honeyman - Eleanor Oliphant se simte excelent 1.0 '{Literatura}.docx

./Galaction:
Galaction - Carte de rugaciuni pentru tot crestinul 1.0 '{Religie}.docx

./Gala Galaction:
Gala Galaction - Moara lui Califar 0.99 '{SF}.docx
Gala Galaction - Roxana. Doctorul Taifun 1.0 '{Dragoste}.docx

./Galina Serebreakova:
Galina Serebreakova - Mireasma de micsandre 1.0 '{Dragoste}.docx

./Garabet Ibraileanu:
Garabet Ibraileanu - Adela 1.0 '{ClasicRo}.docx
Garabet Ibraileanu - Amintiri din copilarie si adolescenta 1.0 '{ClasicRo}.docx
Garabet Ibraileanu - Caracterul specific national in literatura romana 1.0 '{ClasicRo}.docx
Garabet Ibraileanu - Privind viata 1.0 '{ClasicRo}.docx
Garabet Ibraileanu - Scriitori romani si straini 1.0 '{ClasicRo}.docx
Garabet Ibraileanu - Scriitori si curente 1.0 '{ClasicRo}.docx
Garabet Ibraileanu - Spiritul critic in cultura romaneasca 1.0 '{ClasicRo}.docx

./Garci Rodriguez de Montalvo:
Garci Rodriguez de Montalvo - Amadis V1 1.0 '{AventuraIstorica}.docx
Garci Rodriguez de Montalvo - Amadis V2 1.0 '{AventuraIstorica}.docx

./Gardonyi Geza:
Gardonyi Geza - Contract de casatorie 1.0 '{Dragoste}.docx

./Garnett Radcliffe:
Garnett Radcliffe - Banda florilor 1.0 '{Politista}.docx

./Garry Kilworth:
Garry Kilworth - Attica 1.0 '{Tineret}.docx

./Garstea Natan:
Garstea Natan - Lumea lui David 1.0 '{SF}.docx

./Garth Nix:
Garth Nix - Vechiul Regat - V1 Sabriel 2.0 '{AventuraIstorica}.docx
Garth Nix - Vechiul Regat - V2 Lirael 1.0 '{AventuraIstorica}.docx
Garth Nix - Vechiul Regat - V3 Abhorsen 1.0 '{AventuraIstorica}.docx

./Gary Chapman:
Gary Chapman - Cele cinci limbaje ale iubirii 1.0 '{Psihologie}.docx
Gary Chapman - Cele cinci limbaje de iubire ale copiilor 1.0 '{Psihologie}.docx

./Gary Smith:
Gary Smith - Curatirea karmei 0.9 '{Spiritualitate}.docx

./Gary Taubes:
Gary Taubes - De ce te ingrasi 1.0 '{DezvoltarePersonala}.docx

./Gaston Bachelard:
Gaston Bachelard - Poetica reveriei 0.9 '{Psihologie}.docx
Gaston Bachelard - Psihanaliza focului 0.7 '{Psihologie}.docx

./Gaston Leroux:
Gaston Leroux - Misterul camerei galbene 1.0 '{Politista}.docx

./Gaston Renee:
Gaston Renee - Demonul rosu V1 2.0 '{SF}.docx
Gaston Renee - Demonul rosu V2 2.0 '{SF}.docx
Gaston Renee - Demonul rosu V3 2.0 '{SF}.docx
Gaston Renee - Demonul rosu V4 2.0 '{SF}.docx
Gaston Renee - Demonul rosu v5 2.0 '{SF}.docx

./Gayle Brandeis:
Gayle Brandeis - Licitatia lui Flann 0.99 '{Literatura}.docx

./Gayle Forman:
Gayle Forman - Daca as ramane 1.0 '{Dragoste}.docx
Gayle Forman - Daca te-as gasi 1.0 '{Dragoste}.docx
Gayle Forman - Fara mine 1.0 '{Dragoste}.docx

./Gayle Kasper:
Gayle Kasper - Casa de piatra 0.9 '{Dragoste}.docx
Gayle Kasper - Indragostita de patron 0.9 '{Dragoste}.docx

./Gayle Rivers & James Hudson:
Gayle Rivers & James Hudson - Un comando pe doua continente 0.7 '{ActiuneComando}.docx

./Gellu Naum:
Gellu Naum - Despre interior - exterior, dialog cu Sanda Rosescu 0.7 '{Interviu}.docx
Gellu Naum - Zenobia 0.9 '{Dragoste}.docx

./Gelu Bogdan Marin:
Gelu Bogdan Marin - Diavolul si domnisoarele de companie 0.9 '{Diverse}.docx

./Gelu Ionescu:
Gelu Ionescu - Copacul din campie 0.9 '{Jurnalism}.docx

./Gemma Malley:
Gemma Malley - Declaratia 1.0 '{AventuraTineret}.docx

./Gene Edwards:
Gene Edwards - Istoria celor trei regi 0.9 '{Religie}.docx
Gene Edwards - Mai mult decat radicali 0.8 '{Religie}.docx
Gene Edwards - Nepovestita istorie a crestinilor din secolul intai 0.7 '{Religie}.docx
Gene Edwards - Prizonierul din celula a treia 0.99 '{Religie}.docx

./Genevieve Cogman:
Genevieve Cogman - Biblioteca invizibila 1.0 '{SF}.docx

./Genevieve Lyons:
Genevieve Lyons - Decizia Danielei 0.9 '{Dragoste}.docx

./Gene Wolfe:
Gene Wolfe - Cartea Soarelui Lung - V1 Litania soarelui lung V1-2 1.0 '{SF}.docx
Gene Wolfe - Cartea Soarelui Lung - V2 Cartea soarelui nou V1-4 4.0 '{SF}.docx
Gene Wolfe - Cavalerul vrajitor - V1 Cavalerul 1.0 '{Fantasy}.docx
Gene Wolfe - Cavalerul vrajitor - V2 Vrajitorul 1.0 '{Fantasy}.docx
Gene Wolfe - Moartea doctorului Island 1.0 '{SF}.docx
Gene Wolfe - Mut 1.0 '{SF}.docx
Gene Wolfe - Un labrint solar 0.9 '{SF}.docx
Gene Wolfe - Valul 0.9 '{SF}.docx

./Genrikh Altov:
Genrikh Altov - Legendele capitanilor stelari 1.5 '{SF}.docx

./Geoffrey A. Landis:
Geoffrey A. Landis - O plimbare prin soare 0.99 '{SF}.docx

./Geoff Ryman:
Geoff Ryman - Aer 1.0 '{SF}.docx

./Georg Buchner:
Georg Buchner - Leonce si Lena 0.9 '{Teatru}.docx
Georg Buchner - Woyzeck 0.8 '{Teatru}.docx

./George Alin Popescu:
George Alin Popescu - Internetul - Istorie deja 0.3 '{Istorie}.docx

./George Anania:
George Anania - Actiunea Lebada 1.0 '{SF}.docx
George Anania - Cat de mic poate fi infernul 1.0 '{SF}.docx
George Anania - Corsarul de fier V1 1.0 '{ClubulTemerarilor}.docx
George Anania - Corsarul de fier V2 1.0 '{ClubulTemerarilor}.docx
George Anania - O experienta neobisnuita 1.0 '{SF}.docx
George Anania - Ploaia de stele 1.0 '{SF}.docx
George Anania - Test de fiabilitate 1.0 '{SF}.docx

./George Andreescu:
George Andreescu - Iliada, Odiseea, Eneida 1.0 '{Tineret}.docx

./George Ann Jansson:
George Ann Jansson - Tu esti roscata mea 0.9 '{Dragoste}.docx

./George Arion:
George Arion - Detectiv fara voie 1.0 '{Politista}.docx
George Arion - Necuratul din Colga 1.0 '{Politista}.docx

./George Bacovia:
George Bacovia - Plumb 1.0 '{Versuri}.docx

./George Banu:
George Banu - Livada de visini, teatrul nostru 0.9 '{Diverse}.docx

./George Calinescu:
George Calinescu - Bietul Ioanide V1 0.8 '{ClasicRo}.docx
George Calinescu - Bietul Ioanide V2 0.8 '{ClasicRo}.docx
George Calinescu - Cartea nuntii 1.1 '{ClasicRo}.docx
George Calinescu - Catina damnatul 1.0 '{ClubulTemerarilor}.docx
George Calinescu - Enigma Otiliei 1.0 '{ClasicRo}.docx
George Calinescu - Istoria literaturii romane 0.9 '{ClasicRo}.docx
George Calinescu - Specialitati morale 0.99 '{Interzise}.docx

./George Ceausu:
George Ceausu - Instelata aventura 1.0 '{SF}.docx

./George Ciprian:
George Ciprian - Omul cu martoaga 0.9 '{Teatru}.docx

./George Constantin:
George Constantin - Poesii 0.99 '{Versuri}.docx

./George Cosbuc:
George Cosbuc - Balade si idile 0.99 '{Versuri}.docx
George Cosbuc - Cantece de vitejie 1.0 '{Versuri}.docx
George Cosbuc - Din viata 1.0 '{Versuri}.docx
George Cosbuc - Fire de tort 1.0 '{Versuri}.docx
George Cosbuc - Numai una 1.0 '{Versuri}.docx
George Cosbuc - Nunta Zamfirei 0.9 '{Versuri}.docx
George Cosbuc - Regina ostrogotiilor 0.99 '{Versuri}.docx
George Cosbuc - Vestitorii primaverii 0.9 '{Versuri}.docx
George Cosbuc - Ziarul unui pierde vara 1.0 '{Versuri}.docx

./George Dumitru:
George Dumitru - Dreptul de a sta 0.9 '{Literatura}.docx
George Dumitru - Luntre solitara 0.99 '{Versuri}.docx

./George Eliot:
George Eliot - Adam Bede 1.0 '{ClasicSt}.docx
George Eliot - Middlemarch V1 1.0 '{ClasicSt}.docx
George Eliot - Middlemarch V2 1.0 '{ClasicSt}.docx
George Eliot - Middlemarch V3 1.0 '{ClasicSt}.docx
George Eliot - Middlemarch V4 1.0 '{ClasicSt}.docx
George Eliot - Moara de pe Floss 1.0 '{ClasicSt}.docx
George Eliot - Silas Marner 1.0 '{ClasicSt}.docx

./George Friedman:
George Friedman - Urmatorii 100 de ani 1.0 '{MistersiStiinta}.docx

./George Gamow:
George Gamow - Biografia fizicii 1.0 '{Stiinta}.docx

./George Herbert Wells:
George Herbert Wells - Cand se va trezi cel care doarme 2.0 '{SF}.docx
George Herbert Wells - Insula doctorului Moreau 1.1 '{SF}.docx
George Herbert Wells - Istorisirea lui Platner 2.0 '{SF}.docx
George Herbert Wells - Masina timpului 2.1 '{SF}.docx
George Herbert Wells - Omul invizibil 1.1 '{SF}.docx
George Herbert Wells - Opere Alese - V1 Masina timpului 1.0 '{SF}.docx
George Herbert Wells - Opere Alese - V2 Razboiul lumilor 1.0 '{SF}.docx
George Herbert Wells - Opere Alese - V3 Hrana zeilor 1.0 '{SF}.docx
George Herbert Wells - Opere Alese - V4 Oul de cristal 1.0 '{SF}.docx
George Herbert Wells - Povestea raposatului domn Elvesham 2.0 '{SF}.docx
George Herbert Wells - Povestiri 1.0 '{SF}.docx
George Herbert Wells - Primii oameni in luna 1.9 '{SF}.docx
George Herbert Wells - Razboiul lumilor 2.0 '{SF}.docx
George Herbert Wells - Tono Bungay 1.0 '{SF}.docx

./George Meredith:
George Meredith - Diana din Crossways 1.0 '{Dragoste}.docx
George Meredith - Rhoda Fleming 1.0 '{Dragoste}.docx
George Meredith - Sandra Belloni 0.9 '{Dragoste}.docx

./George Mihail Zamfirescu:
George Mihail Zamfirescu - Madona cu trandafiri 1.0 '{Literatura}.docx
George Mihail Zamfirescu - Maidanul cu dragoste 1,0 '{Literatura}.docx

./George Mihalache:
George Mihalache - Cand Tanase era rege 0.8 '{Diverse}.docx

./George Muresan:
George Muresan - Opere(te) (in)complete 0.99 '{ProzaScurta}.docx

./George Orwell:
George Orwell - 1984 1.9 '{Literatura}.docx
George Orwell - Ferma animalelor 3.0 '{Literatura}.docx
George Orwell - Vagabond prin Paris si Londra 1.0 '{Literatura}.docx

./George Padure:
George Padure - O viata cat zece 1.0 '{Biografie}.docx

./George Pietraru:
George Pietraru - Miu haiducul. Intoarcerea lui Miu Haiducu 5.0 '{ClubulTemerarilor}.docx

./George R. R. Martin:
George R. R. Martin - Adanci, tot mai adanci tunelurile 1.0 '{AventuraIstorica}.docx
George R. R. Martin - Antologie - Razboinicii V1 1.0 '{AventuraIstorica}.docx
George R. R. Martin - Antologie - Razboinicii V2 1.0 '{AventuraIstorica}.docx
George R. R. Martin - Cantec de Gheata si Foc - V1 Urzeala tronurilor V1 4.0 '{AventuraIstorica}.docx
George R. R. Martin - Cantec de Gheata si Foc - V1 Urzeala tronurilor V2 4.0 '{AventuraIstorica}.docx
George R. R. Martin - Cantec de Gheata si Foc - V2 Inclestarea regilor V1 4.0 '{AventuraIstorica}.docx
George R. R. Martin - Cantec de Gheata si Foc - V2 Inclestarea regilor V2 4.0 '{AventuraIstorica}.docx
George R. R. Martin - Cantec de Gheata si Foc - V3 Iuresul sabiilor V1 4.0 '{AventuraIstorica}.docx
George R. R. Martin - Cantec de Gheata si Foc - V3 Iuresul sabiilor V2 4.0 '{AventuraIstorica}.docx
George R. R. Martin - Cantec de Gheata si Foc - V3 Iuresul sabiilor V3 4.0 '{AventuraIstorica}.docx
George R. R. Martin - Cantec de Gheata si Foc - V4 Festinul ciorilor V1 4.0 '{AventuraIstorica}.docx
George R. R. Martin - Cantec de Gheata si Foc - V4 Festinul ciorilor V2 4.0 '{AventuraIstorica}.docx
George R. R. Martin - Cantec de Gheata si Foc - V5 Dansul dragonilor V1 1.0 '{AventuraIstorica}.docx
George R. R. Martin - Cantec de Gheata si Foc - V5 Dansul dragonilor V2 1.0 '{AventuraIstorica}.docx
George R. R. Martin - Cavalerul misterios 1.0 '{AventuraIstorica}.docx
George R. R. Martin - Foc si sange 1.0 '{AventuraIstorica}.docx
George R. R. Martin - Fuga vanatorului 2.0 '{AventuraIstorica}.docx
George R. R. Martin - Furtuna pe Windhaven 2.0 '{AventuraIstorica}.docx
George R. R. Martin - Lumina ce se stinge 3.0 '{AventuraIstorica}.docx
George R. R. Martin - Peregrinarile lui Tuf 1.0 '{AventuraIstorica}.docx
George R. R. Martin - Regii nisipurilor 1.0 '{AventuraIstorica}.docx
George R. R. Martin - Stare de asediu 1.1 '{AventuraIstorica}.docx
George R. R. Martin - Weekend intr-o zona de razboi 2.1 '{AventuraIstorica}.docx
George R. R. Martin - Wild Cards 1.0 '{AventuraIstorica}.docx
George R. R. Martin - Zburatorii noptii 2.0 '{AventuraIstorica}.docx

./George R. R. Walter:
George R. R. Walter - Influenta limbajului pozitiv 0.99 '{AventuraIstorica}.docx

./George Radu:
George Radu - Funia de nisip 0.7 '{Literatura}.docx

./George Ricus:
George Ricus - Hotul de paratraznete 2.0 '{SF}.docx
George Ricus - O viata aparte 1.0 '{SF}.docx

./George Rogoz:
George Rogoz - Legea 1 0.9 '{SF}.docx

./George S. Clason:
George S. Clason - Cel mai bogat om din Babilon 0.9 '{DezvoltarePersonala}.docx

./George Sand:
George Sand - Amintiri din Berry 0.9 '{Dragoste}.docx
George Sand - Doamnele verzi 0.8 '{Dragoste}.docx
George Sand - Ea si el 0.9 '{Dragoste}.docx
George Sand - Etienne Depardieu 0.4 '{Dragoste}.docx
George Sand - Indiana 0.6 '{Dragoste}.docx
George Sand - Lelia 1.0 '{Dragoste}.docx
George Sand - Mauprat 1.0 '{Dragoste}.docx
George Sand - Mesterul Pierre Huguenin 0.5 '{Dragoste}.docx
George Sand - Ultima dragoste 1.0 '{Dragoste}.docx
George Sand - Valentina 0.8 '{Dragoste}.docx

./George Sarry:
George Sarry - Viata mea, amintiri din inchisoare si din libertate 1.0 '{Comunism}.docx

./Georges Blond:
Georges Blond - Debarcarea 1.0 '{Razboi}.docx
Georges Blond - Glorie si frumusete 1.0 '{Dragoste}.docx
Georges Blond - Supravietuitorul din Pacific 1.0 '{Razboi}.docx

./Georges Charpak & Henri Broch:
Georges Charpak & Henri Broch - Lectii de vrajitorie. Stiinta si paranormalul 0.8 '{Spiritualitate}.docx

./Georgescu Matei:
Georgescu Matei - Introducere in teoria negocierii 0.99 '{DezvoltarePersonala}.docx

./Georges Minois:
Georges Minois - Istoria infernurilor 1.0 '{Istorie}.docx

./George Soros:
George Soros - Despre globalizare 0.9 '{Economie}.docx

./George Sovu:
George Sovu - Dans in foisor 1.0 '{AventuraTineret}.docx
George Sovu - Declaratie de dragoste 1.0 '{AventuraTineret}.docx
George Sovu - Dimineata iubirii 1.0 '{AventuraTineret}.docx
George Sovu - Extemporal la dirigentie 1.0 '{AventuraTineret}.docx
George Sovu - Jarul din palma 2.0 '{AventuraTineret}.docx
George Sovu - Liceenii 5.0 '{AventuraTineret}.docx
George Sovu - Liceenii in alerta 1.0 '{AventuraTineret}.docx
George Sovu - Liceenii rock'n roll 1.0 '{AventuraTineret}.docx
George Sovu - Ochii timpului 1.0 '{AventuraTineret}.docx
George Sovu - Tandrete 1.0 '{AventuraTineret}.docx

./Georges Simenon:
Georges Simenon - Asta-i Felicie! 2.0 '{Politista}.docx
Georges Simenon - Cainele galben 2.0 '{Politista}.docx
Georges Simenon - Capcana lui Maigret 2.0 '{Politista}.docx
Georges Simenon - Careul de asi 2.0 '{Politista}.docx
Georges Simenon - Cazul Louise Laboine 2.0 '{Politista}.docx
Georges Simenon - Cazul Saint Fiacre 2.0 '{Politista}.docx
Georges Simenon - Cine a ucis-o pe Cecile 2.0 '{Politista}.docx
Georges Simenon - Comisarul Maigret a fost pradat 1.0 '{Politista}.docx
Georges Simenon - Dansatoarea de la Gai Moulin 2.0 '{Politista}.docx
Georges Simenon - Domnul Gallet a decedat 2.0 '{Politista}.docx
Georges Simenon - Ecluza nr.1 1.0 '{Politista}.docx
Georges Simenon - Esecul lui Maigret 2.0 '{Politista}.docx
Georges Simenon - Inspectorul cadavre 2.0 '{Politista}.docx
Georges Simenon - Liberty bar 2.0 '{Politista}.docx
Georges Simenon - Maigret 2.0 '{Politista}.docx
Georges Simenon - Maigret calatoreste 2.1 '{Politista}.docx
Georges Simenon - Maigret ezita 2.0 '{Politista}.docx
Georges Simenon - Maigret in Arizona 2.0 '{Politista}.docx
Georges Simenon - Maigret inchiriaza o camera 1.0 '{Politista}.docx
Georges Simenon - Maigret intra in ring 2.0 '{Politista}.docx
Georges Simenon - Maigret isi pierde cumpatul 2.0 '{Politista}.docx
Georges Simenon - Maigret la ministru 1.0 '{Politista}.docx
Georges Simenon - Maigret la New York 2.0 '{Politista}.docx
Georges Simenon - Maigret la Picratt&s 2.0 '{Politista}.docx
Georges Simenon - Maigret la Vichy 2.0 '{Politista}.docx
Georges Simenon - Maigret se amuza 1.0 '{Politista}.docx
Georges Simenon - Maigret se apara 2.1 '{Politista}.docx
Georges Simenon - Maigret se infurie 2.0 '{Politista}.docx
Georges Simenon - Maigret se inseala 1.0 '{Politista}.docx
Georges Simenon - Maigret se teme 2.0 '{Politista}.docx
Georges Simenon - Maigret si batrana doamna 2.0 '{Politista}.docx
Georges Simenon - Maigret si cadavrul fara cap 2.0 '{Politista}.docx
Georges Simenon - Maigret si cazul Nahour 2.0 '{Politista}.docx
Georges Simenon - Maigret si clientul de sambata 1.0 '{Politista}.docx
Georges Simenon - Maigret si crima din ziar 2.0 '{Politista}.docx
Georges Simenon - Maigret si domnul Charles 2.1 '{Politista}.docx
Georges Simenon - Maigret si fantoma 2.0 '{Politista}.docx
Georges Simenon - Maigret si femeia nebuna 2.0 '{Politista}.docx
Georges Simenon - Maigret si flamanzii 2.0 '{Politista}.docx
Georges Simenon - Maigret si gangsterii 2.0 '{Politista}.docx
Georges Simenon - Maigret si hotul lenes 2.0 '{Politista}.docx
Georges Simenon - Maigret si informatorul 2.1 '{Politista}.docx
Georges Simenon - Maigret si martorii recalcitranti 1.0 '{Politista}.docx
Georges Simenon - Maigret si moartea Louisei 2.0 '{Politista}.docx
Georges Simenon - Maigret si mortul 2.0 '{Politista}.docx
Georges Simenon - Maigret si omul dublu 2.0 '{Politista}.docx
Georges Simenon - Maigret si prietenul din copilarie 2.0 '{Politista}.docx
Georges Simenon - Maigret si scoala crimei 2.0 '{Politista}.docx
Georges Simenon - Maigret si ucigasul 2.0 '{Politista}.docx
Georges Simenon - Maigret si vagabondul 1.0 '{Politista}.docx
Georges Simenon - Maigret si vagabondul singuratic 2.0 '{Politista}.docx
Georges Simenon - Mania lui Maigret 2.0 '{Politista}.docx
Georges Simenon - Memoriile lui Maigret 2.0 '{Politista}.docx
Georges Simenon - Nebunul din Bergerac 2.0 '{Politista}.docx
Georges Simenon - O crima in Olanda 1.0 '{Politista}.docx
Georges Simenon - Picpus 2.0 '{Politista}.docx
Georges Simenon - Pietr letonul 1.0 '{Politista}.docx
Georges Simenon - Pisica 1.0 '{Politista}.docx
Georges Simenon - Pivnitele hotelului Majestic 1.0 '{Politista}.docx
Georges Simenon - Prietena doamnei Maigret 2.0 '{Politista}.docx
Georges Simenon - Prietenul meu Maigret 2.1 '{Politista}.docx
Georges Simenon - Prima ancheta a lui Maigret 2.0 '{Politista}.docx
Georges Simenon - Rabdarea lui Maigret 1.0 '{Politista}.docx
Georges Simenon - Raspantia mortii 2.0 '{Politista}.docx
Georges Simenon - Revolverul lui Maigret 2.0 '{Politista}.docx
Georges Simenon - Spanzuratul de la Saint Pholien 2.0 '{Politista}.docx
Georges Simenon - Taverna de pe malul Senei 1.0 '{Politista}.docx
Georges Simenon - Umbra chinezeasca 2.0 '{Politista}.docx
Georges Simenon - Vacanta lui Maigret 1.0 '{Politista}.docx

./George Toparceanu:
George Toparceanu - Amintiri din luptele de la Turtucaia 1.0 '{ClasicRo}.docx
George Toparceanu - Balade vesele si triste 1.0 '{Versuri}.docx
George Toparceanu - Minunile sfantului Sisoe 0.9 '{ClasicRo}.docx
George Toparceanu - Parodii originale 0.99 '{Versuri}.docx
George Toparceanu - Postume 0.99 '{Versuri}.docx
George Toparceanu - Scrisori fara adresa 1.0 '{ClasicRo}.docx

./Georgette Heyer:
Georgette Heyer - Pentru dragostea lui Cressy 0.9 '{Dragoste}.docx

./George Uba:
George Uba - Taie-mi o felie de dragoste 0.6 '{Literatura}.docx

./George Vasilievici:
George Vasilievici - Viseptol 1.0 '{Literatura}.docx

./George Vulturescu:
George Vulturescu - Tratat despre ochiul orb 0.9 '{Versuri}.docx

./Georgia Harris:
Georgia Harris - Insula centaurului 0.2 '{Dragoste}.docx

./Georgia Hunter:
Georgia Hunter - Supravietuitorii 1.0 '{Razboi}.docx

./Georgiana Danet:
Georgiana Danet - Femeia si astrele 0.99 '{Spiritualitate}.docx

./Georgina Viorica Rogoz:
Georgina Viorica Rogoz - Anotimpul sirenelor 1.0 '{SF}.docx
Georgina Viorica Rogoz - Draculestii 1.0 '{SF}.docx
Georgina Viorica Rogoz - Metamorfoza lui R4-211919 1.0 '{SF}.docx
Georgina Viorica Rogoz - Sa nu afle Aladin 1.0 '{SF}.docx

./Georg Von Viebahn:
Georg Von Viebahn - Care este adevarata credinta 0.99 '{Religie}.docx

./Geo Savulescu:
Geo Savulescu - Lucian Blaga - Filosofia prin metafore 0.9 '{Filozofie}.docx

./Gerald Messadie:
Gerald Messadie - 29 de zile pana la sfarsitul lumi 1.0 '{AventuraIstorica}.docx
Gerald Messadie - Furtuni pe Nil - V1 Ochiul lui Nefertiti 1.0 '{AventuraIstorica}.docx
Gerald Messadie - Furtuni pe Nil - V2 Mastile lui Tutankhamon 1.0 '{AventuraIstorica}.docx
Gerald Messadie - Furtuni pe Nil - V3 Triumful lui Seth 1.0 '{AventuraIstorica}.docx
Gerald Messadie - Iuda preaiubitul 1.0 '{AventuraIstorica}.docx
Gerald Messadie - Moise - V1 Printul fara coroana 1.0 '{AventuraIstorica}.docx
Gerald Messadie - Moise - V2 Profet intemeietor 1.0 '{AventuraIstorica}.docx

./Gerald O'Farrell:
Gerald O'Farrell - Mormantul lui Tutankhamon 1.0 '{MistersiStiinta}.docx.docx

./Gerard de Villiers:
Gerard de Villiers - Justitiarul - Atacul de La New Jersey 0.8 '{ActiuneComando}.docx
Gerard de Villiers - Justitiarul - Razboiul Sfant incepe 0.99 '{ActiuneComando}.docx
Gerard de Villiers - Sas 001 - Sas la Istanbul 2.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 002 - Tradare in CIA 2.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 003 - Operatiunea Apocalips 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 004 - Executie la Rio 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 005 - Rendez Vous la San Francisco 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 006 - Dosarul Kennedy 4.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 007 - Condamnat la moarte 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 008 - Ciclonul ucigas 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 009 - La vest de Ierusalim 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 010 - Aurul de pe Raul Kwai 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 011 - Magie neagra la New York 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 012 - Vaduvele din Hong Kong 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 013 - Sirena diabolica 4.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 014 - Spanzuratii din Bagdad 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 015 - Pantera de la Hollywood 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 016 - Misterul din Pago Pago 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 017 - Bali, paradis mortal 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 018 - Statuia ucigasa 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 019 - Ciclon la ONU 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 021 - Balul contesei Adler 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 022 - Proscrisii din Ceylon 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 025 - Moarte in Afganistan 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 027 - Safari in La Paz 2.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 029 - Berlin, zidul mortii 2.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 031 - Ingerul negru din Montevideo 4.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 034 - Omorati-l pe Henry Kissinger 2.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 035 - Ruleta cambodgiana 2.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 037 - Viespar in Angola 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 038 - Ostaticii din Tokyo 2.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 039 - Teroare la Santiago 2.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 040 - Vrajitorii din Lisabona 2.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 042 - Disparut la Singapore 2.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 043 - Numaratoare inversa in Rhodesia 2.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 045 - Comoara negusului 4.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 050 - Primavara la Varsovia 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 051 - De veghe in Israel 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 054 - Sa vezi Malta si apoi sa mori 2.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 058 - Joc murdar la Budapesta 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 059 - Macel la Abu Dhabi 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 060 - Teroare la San Salvador 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 062 - Ochiul vaduvei 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 063 - Arme pentru Khartoum 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 064 - Tornada asupra Manilei 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 066 - Obiectiv Reagan 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 068 - Comandou la Tunis 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 069 - Ucigasul din Miami 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 070 - Filiera bulgara 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 071 - Aventura in Surinam 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 072 - Ambuscada in trecatoarea Khybr 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 074 - Nebunii din Baalbek 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 075 - Furiosii din Amsterdam 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 077 - Blonda din Pretoria 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 078 - Vaduva aiatolahului 0.9 '{ActiuneComando}.docx
Gerard de Villiers - Sas 079 - Haituit in Peru 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 080 - Afacerea Kirsnov 2.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 082 - Dans macabru la Belgrad 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 083 - Lovitura de stat in Yemen 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 087 - Ostaticul din Oman 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 088 - Escala la Gibraltar 2.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 089 - Joc periculos in Sierra Leone 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 091 - Amazoanele din Pyongyang 1.1 '{ActiuneComando}.docx
Gerard de Villiers - Sas 092 - Teroristii din Bruxelles 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 093 - Viza pentru Cuba 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 096 - Suspectul din Leningrad 2.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 098 - Revolta in Birmania 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 099 - Complot impotriva lui Gorbaciov 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 101 - Un agent CIA in Congo 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 102 - Teroarea Khmerilor rosii 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 103 - Razbunarea lui Saddam Hussein 2.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 104 - Manipulare la Zagreb 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 105 - KGB contra KGB 0.8 '{ActiuneComando}.docx
Gerard de Villiers - Sas 108 - Lovitura de stat la Tripoli 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 110 - Numele de cod El Diablo 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 111 - Arme pentru terorism 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 112 - Razbunare la Beirut 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 113 - Atentat la Casa Alba 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 115 - Crucea apartheitului 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 117 - Masacrul de la Marrakech 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 118 - Ostaticul din Triunghiul de Aur 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 119 - CIA contraataca 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 120 - El Coyote si cartelul drogurilor 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 121 - Sadam Hussein tradat de fiii sai 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 123 - Capcana cecena 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 124 - Vanati-l pe Radovan Karadzici 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 125 - Aventura in Afganistan 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 126 - Scrisoare pentru Clinton 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 128 - Adio Zair, adio Mobutu 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 129 - Yggdrasil - Manipulare satanica 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 130 - Moarte in Jamaica 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 131 - Nebunia lui Saddam 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 132 - Un spion la Vatican 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 133 - Albania, misiune imposibila 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 134 - Sursa Yahalom 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 136 - Bombe asupra Belgradului 2.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 137 - Pista Monte Carlo - Kremlin 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 138 - Marea iubire a colonelului Chang 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 139 - America in pericol 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 139 - Jihad 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 140 - Ancheta asupra unui genocid 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 141 - Ostaticul din Jolo 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 142 - Atentat asupra Papei 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 143 - Armaghedon 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 144 - Capcana mortala 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 144 - Li Sha-Tin trebuie sa moara 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 145 - Regele nebun al Nepalului 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 146 - Protectie pentru Teddy Bear 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 147 - Afacerea Karina 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 148 - Bin Laden haituit 2.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 151 - Aurul retelei Al-Qaida 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 152 - Pact cu diavolul la Belgrad 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 154 - Reteaua Istanbul 0.6 '{ActiuneComando}.docx
Gerard de Villiers - Sas 159 - Misiune in Cuba 0.8 '{ActiuneComando}.docx
Gerard de Villiers - Sas 161 - Programul 111 V1 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 162 - Programul 111 V2 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 163 - Comoara lui Saddam V1 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 164 - Comoara lui Saddam V2 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 166 - Libanul rosu 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 167 - Poloniu 210 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 168 - Fugarul din Phenian V1 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 169 - Fugarul din Phenian V2 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 170 - Ostatic la talibani 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 171 - Agenda Kosovo 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 172 - Intoarcere la Shangri La 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 173 - Al Qaida ataca! V1 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 174 - Al Qaida ataca! V2 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 175 - Omorati-l pe Dalai Lama 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 176 - Primavara la Tbilisi 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 177 - Piratii 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 178 - Batalia pentru rachetele S-300 V1 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 179 - Batalia pentru rachetele S-300 V2 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 180 - Capcana din Bangkok 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 181 - Lista Hariri 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 182 - Filiera elvetiana 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 183 - Renegatul V1 2.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 184 - Renegatul V2 2.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 185 - Guineea salbatica 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 186 - Stapanul randunelelor 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 187 - Bun venit la Nouakchott 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 188 - Dragonul rosu V1 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 189 - Dragonul rosu V2 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 190 - Ciudad Juarez 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 193 - Drumul Damascului V1 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 194 - Drumul Damascului V2 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 195 - Panica la Bamako 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 196 - Frumoasa Dunare rosie 1.0 '{ActiuneComando}.docx
Gerard de Villiers - Sas 197 - Fantomele din Lockerbie 1.0 '{ActiuneComando}.docx

./Gerard Klein:
Gerard Klein - Chirurgii planetari 1.0 '{SF}.docx
Gerard Klein - De-a v-ati ascunselea 1.0 '{SF}.docx
Gerard Klein - Gambitul stelelor 0.99 '{SF}.docx
Gerard Klein - Legea talionului 0.9 '{SF}.docx
Gerard Klein - Memorie vie, memorie moarta 0.99 '{SF}.docx
Gerard Klein - Orasele 1.0 '{SF}.docx
Gerard Klein - Planeta cu sapte masti 1.0 '{SF}.docx
Gerard Klein - Povestiri de parca ar fi 0.99 '{SF}.docx
Gerard Klein - Reabilitare 0.99 '{SF}.docx
Gerard Klein - Sceptrul hazardului 0.99 '{SF}.docx
Gerard Klein - Seniorii razboiului 1.0 '{CalatorieinTimp}.docx
Gerard Klein - Timpul nu are miros 1.0 '{CalatorieinTimp}.docx

./Gerard Majax:
Gerard Majax - Magicienii 0.99 '{MistersiStiinta}.docx

./Gerard Stembridge:
Gerard Stembridge - Detectivul delicat 0.99 '{Teatru}.docx

./Germaine Tillion:
Germaine Tillion - Ravensbruch 1.0 '{Politica}.docx

./German I. Matveev:
German I. Matveev - Scorpionul 1.0 '{Suspans}.docx

./Geza Szocs:
Geza Szocs - Busurmanii din crangul de jos 1.9 '{Teatru}.docx

./Gh. Karaslavov:
Gh. Karaslavov - Tango 1.0 '{Literatura}.docx

./Ghelasie Gheorghe:
Ghelasie Gheorghe - Retetele medicinii isihaste 0.7 '{Spiritualitate}.docx

./Ghennadi Maximovici:
Ghennadi Maximovici - Ultima confesiune a lui Louis Qufault 0.99 '{Politista}.docx

./Ghennadi Melnikov:
Ghennadi Melnikov - Dimineata senina dupa o lunga noapte 0.99 '{Diverse}.docx

./Gheorghe Aldea:
Gheorghe Aldea - Alerta la Militie 1.0 '{Politista}.docx

./Gheorghe Andreica:
Gheorghe Andreica - Cu ghiozdanul la inchisoare si adio inchisoare din Sighetul Marmatiei 0.8 '{Comunism}.docx
Gheorghe Andreica - Marturii din iadul temnitelor comuniste 0.99 '{Comunism}.docx
Gheorghe Andreica - Masacrarea studentimii romane 0.8 '{Comunism}.docx

./Gheorghe Asachi:
Gheorghe Asachi - Corbul si vulpea 1.0 '{Fabule}.docx
Gheorghe Asachi - Valea alba 1.0 '{Literatura}.docx

./Gheorghe Barbul:
Gheorghe Barbul - Memorial Antonescu - Al treilea om al Axei 1.0 '{Istorie}.docx

./Gheorghe Braescu:
Gheorghe Braescu - Unde sta norocul 0.9 '{ProzaScurta}.docx

./Gheorghe Buzoianu:
Gheorghe Buzoianu - Cadouri pentru o matusa 1.0 '{Politista}.docx
Gheorghe Buzoianu - Capcana 1.0 '{Politista}.docx
Gheorghe Buzoianu - Operatiunea Ghinturesti 1.0 '{Politista}.docx
Gheorghe Buzoianu - Prin labirint 2.0 '{Politista}.docx

./Gheorghe Coman:
Gheorghe Coman - Psihologie si teorie cuantica 0.8 '{Psihologie}.docx

./Gheorghe Crisan:
Gheorghe Crisan - O viata pentru acest pamant 0.9 '{SF}.docx

./Gheorghe Doran:
Gheorghe Doran - Visul de noapte al apelor 0.99 '{Versuri}.docx

./Gheorghe Florescu:
Gheorghe Florescu - Confesiunile unui cafegiu 2.0 '{Literatura}.docx

./Gheorghe Funar:
Gheorghe Funar - Actualitatea geniului Mihai Eminescu 0.99 '{Interzise}.docx

./Gheorghe Gane:
Gheorghe Gane - Valea Antari 0.8 '{Diverse}.docx

./Gheorghe Gorun:
Gheorghe Gorun - Insemnarile unui sergent de graniceri 0.8 '{ProzaScurta}.docx

./Gheorghe Grigurcu & Laszlo Alexandru & Ovidiu Pecican:
Gheorghe Grigurcu & Laszlo Alexandru & Ovidiu Pecican - Vorbind 0.99 '{Convorbiri}.docx

./Gheorghe Hibovski:
Gheorghe Hibovski - Circus maximus 0.9 '{Versuri}.docx
Gheorghe Hibovski - Cloaca maxima 0.9 '{Versuri}.docx

./Gheorghe I. Bratianu:
Gheorghe I. Bratianu - O enigma si un miracol istoric, poporul roman 1.0 '{Istorie}.docx

./Gheorghe Iliescu:
Gheorghe Iliescu - Conacul spionat 1.0 '{ClubulTemerarilor}.docx

./Gheorghe Ivanoiu:
Gheorghe Ivanoiu - Soldatul Ion 0.9 '{ActiuneRazboi}.docx

./Gheorghe Mencinicopschi:
Gheorghe Mencinicopschi - Si noi ce mai mancam ca sa slabim V2 0.6 '{Alimentatie}.docx

./Gheorghe Paschia:
Gheorghe Paschia - Din misterele Odesei 0.99 '{MistersiStiinta}.docx

./Gheorghe Paun:
Gheorghe Paun - Paianjenii 0.9 '{SF}.docx
Gheorghe Paun - Sfera paralela 1.0 '{SF}.docx

./Gheorghe Rogoz:
Gheorghe Rogoz - Bo 0.9 '{SF}.docx
Gheorghe Rogoz - Cadoul 0.99 '{SF}.docx
Gheorghe Rogoz - Cristerra 0.99 '{SF}.docx
Gheorghe Rogoz - Experimentul 0.99 '{SF}.docx
Gheorghe Rogoz - Factorul haos 0.9 '{SF}.docx
Gheorghe Rogoz - Legenda OMU-rilor 0.9 '{SF}.docx
Gheorghe Rogoz - Povestea pensionarului incredul 0.7 '{SF}.docx
Gheorghe Rogoz - Povestea tarii Nucalanoi 0.8 '{SF}.docx
Gheorghe Rogoz - Virusul 0.9 '{SF}.docx

./Gheorghe Sasarman:
Gheorghe Sasarman - Himera 1.0 '{SF}.docx
Gheorghe Sasarman - Operatiunea K3 0.99 '{SF}.docx
Gheorghe Sasarman - Oracolul 1.0 '{SF}.docx
Gheorghe Sasarman - Proba tacerii 0.99 '{SF}.docx
Gheorghe Sasarman - Varianta balcanica imbunatatita 0.9 '{SF}.docx

./Gheorghe Schwartz:
Gheorghe Schwartz - Cei O Suta - V1 Anabasis 1.0 '{AventuraIstorica}.docx
Gheorghe Schwartz - Cei O Suta - V2 Ecce Homo 1.0 '{AventuraIstorica}.docx
Gheorghe Schwartz - Cei O Suta - V4 Mana alba 1.0 '{AventuraIstorica}.docx

./Gheorghe Sireteanu:
Gheorghe Sireteanu - Comoara craisorului 1.0 '{IstoricaRo}.docx

./Gheorghe Tanasescu:
Gheorghe Tanasescu - Erou la Cotul Donului 1.0 '{Razboi}.docx
Gheorghe Tanasescu - Prizonier la Cotul Donului 1.0 '{Razboi}.docx

./Gheorghe Vladutescu:
Gheorghe Vladutescu - Incercare de utopie 0.7 '{Diverse}.docx

./Gheorghi Martinov:
Gheorghi Martinov - Substituirea 0.99 '{SF}.docx

./Gheorghios S. Kroustalakis:
Gheorghios S. Kroustalakis - Batranul Porfirie 0.99 '{Religie}.docx

./Gheorghita Coser:
Gheorghita Coser - Astrologie - autocunoastere 0.8 '{Spiritualitate}.docx

./Gherasim Domide:
Gherasim Domide - Convorbiri cu actorul Ion Sasaran 0.9 '{Convorbiri}.docx

./Gherasim Firmilian & Ion Vladuca:
Gherasim Firmilian & Ion Vladuca - Ortodoxia si eroarea evolutionista 1.0 '{MistersiStiinta}.docx

./Ghidul misoginului:
Ghidul misoginului - Mic tratat de calomnii 1.0 '{Umor}.docx

./Gianfranco de Turris:
Gianfranco de Turris - Linistea universului 1.0 '{SF}.docx

./Gianni Rodari:
Gianni Rodari - Aventurile lui Cepelica 0.99 '{BasmesiPovesti}.docx

./Gib Mihaescu:
Gib Mihaescu - Bratul Andromedei 0.5 '{ClasicRo}.docx
Gib Mihaescu - Donna Alba 2.0 '{ClasicRo}.docx
Gib Mihaescu - Femeia de ciocolata 1.0 '{ClasicRo}.docx
Gib Mihaescu - Rusoaica 1.0 '{ClasicRo}.docx
Gib Mihaescu - Un calator 1.0 '{ClasicRo}.docx
Gib Mihaescu - Zilele si noptile unui student intarziat 1.0 '{ClasicRo}.docx

./Gib Mlhaescu:
Gib Mlhaescu - Troita 0.9 '{ClasicRo}.docx

./Gibson Lavery:
Gibson Lavery - Garnizoana mortii 0.99 '{ActiuneComando}.docx

./Gilbert Adair:
Gilbert Adair - O carte inchisa 1.0 '{Thriller}.docx
Gilbert Adair - Visatorii 2.0 '{Literatura}.docx

./Gilbert Cesbron:
Gilbert Cesbron - O albina se zbate in fereastra 1.0 '{Dragoste}.docx

./Gilbert Sinoue:
Gilbert Sinoue - Tacerea lui Dumnezeu 1.0 '{Politista}.docx

./Giles Whittell:
Giles Whittell - Podul spionilor 1.0 '{Suspans}.docx

./Gilles Schlesser:
Gilles Schlesser - Trei cioburi de eternitate 0.99 '{Suspans}.docx

./Gillian Flynn:
Gillian Flynn - Dupa fapta si rasplata 1.0 '{Thriller}.docx
Gillian Flynn - Fata disparuta 1.0 '{Thriller}.docx
Gillian Flynn - Obiecte ascutite 1.0 '{Thriller}.docx

./Gill Paul:
Gill Paul - Sotia secreta 1.0 '{Dragoste}.docx

./Gina Douglas:
Gina Douglas - Mariaj pentru o viata 0.9 '{Dragoste}.docx

./Gin Phillips:
Gin Phillips - Jocul supravietuirii 1.0 '{Thriller}.docx

./Giordano Bruno:
Giordano Bruno - Despre infinit, univers si lumi 0.9 '{Filozofie}.docx

./Giorgio Bassani:
Giorgio Bassani - Gradinile Finzi Contini 0.9 '{Literatura}.docx

./Giorgio Faletti:
Giorgio Faletti - Cu ochii altuia 0.99 '{Thriller}.docx
Giorgio Faletti - Dincolo de un destin evident 1.0 '{Diverse}.docx
Giorgio Faletti - Eu ucid 1.0 '{Thriller}.docx

./Giovanni Arpino:
Giovanni Arpino - La umbra colinelor 1.0 '{Literatura}.docx
Giovanni Arpino - Parfum de femeie 0.99 '{Thriller}.docx
Giovanni Arpino - Un delict de onoare 2.0 '{Literatura}.docx

./Giovanni Boccaccio:
Giovanni Boccaccio - Decameronul 1.0 '{ClasicSt}.docx

./Giovanni Papini:
Giovanni Papini - Cartea neagra 0.9 '{Literatura}.docx
Giovanni Papini - Gog 0.99 '{Literatura}.docx
Giovanni Papini - Martorii patimilor 0.99 '{Literatura}.docx
Giovanni Papini - Poveste complet absurda 0.9 '{Literatura}.docx

./Giovanni Verga:
Giovanni Verga - Eva sotul Elenei 0.6 '{Literatura}.docx

./Giuseppe Bandi:
Giuseppe Bandi - Marsul celor 1000 0.9 '{Diverse}.docx

./Giuseppe Culicchia:
Giuseppe Culicchia - Tara minunilor 0.9 '{Literatura}.docx

./Giuseppe Tomasi Di Lampedusa:
Giuseppe Tomasi Di Lampedusa - Ghepardul 1.0 '{Literatura}.docx

./Gladys Moreton:
Gladys Moreton - Te vei intoarce 0.7 '{Dragoste}.docx

./Gleb Dragan:
Gleb Dragan - Deportatii 1.0 '{Razboi}.docx

./Glen Cook:
Glen Cook - Compania Neagra - V1 Compania neagra 1.0 '{SF}.docx
Glen Cook - Compania Neagra - V2 Umbre staruitoare 1.0 '{SF}.docx

./Glendon Swarthout:
Glendon Swarthout - Binecuvantati animalele si copii 0.9 '{Aventura}.docx

./Glenna Mcreynolds:
Glenna Mcreynolds - Ingerul razbunator 0.8 '{Romance}.docx
Glenna Mcreynolds - Paradisul dragonului 0.99 '{Dragoste}.docx

./Glenn Cooper:
Glenn Cooper - Biblioteca mortilor 1.0 '{AventuraIstorica}.docx

./Gligor Hasa:
Gligor Hasa - Sceptrul lui Decebal 1.0 '{AventuraIstorica}.docx

./Gloria Chadwick:
Gloria Chadwick - Descoperirea vietilor trecute 0.6 '{Spiritualitate}.docx

./Gloria Tiffin:
Gloria Tiffin - Din dragoste pentru Nicky 0.99 '{Dragoste}.docx

./Golnaz Hashemzadeh Bonde:
Golnaz Hashemzadeh Bonde - Ceea ce datoram 1.0 '{SF}.docx

./Gonzalo Ballaster:
Gonzalo Ballaster - Fragmente de apocalipsa 0.99 '{Literatura}.docx

./Gopi Krishna:
Gopi Krishna - Energia evolutiva in om - Kundalini 0.7 '{Spiritualitate}.docx

./Gordon Dahlquist:
Gordon Dahlquist - Devoratorii de vise 1.0 '{Thriller}.docx

./Gordon Korman:
Gordon Korman - Masterminds - V1 Minti geniale 1.0 '{AventuraTineret}.docx

./Gordon Landsborough:
Gordon Landsborough - Vulpile desertului 2.1 '{ActiuneRazboi}.docx

./Gordon R. Dickson:
Gordon R. Dickson - Furtuna de timp 0.9 '{CalatorieinTimp}.docx
Gordon R. Dickson - Golful delfinului 0.99 '{SF}.docx
Gordon R. Dickson - Lup si fier 2.0 '{SF}.docx

./Gordon Roderick & Brian Williams:
Gordon Roderick & Brian Williams - Tunele - V3 In cadere libera 1.0 '{Tineret}.docx

./Gordon Thomas:
Gordon Thomas - Istoria secreta a Mossadului 1.0 '{Suspans}.docx

./Gottfried August Btirger:
Gottfried August Btirger - Uimitoarele calatorii si aventuri pe uscat ale baronului Munchhausen 3.0 '{Tineret}.docx

./Gottfried Kluge:
Gottfried Kluge - Cavalerii Ordinului Negru 1.0 '{ActiuneRazboi}.docx

./Gottfried Mayerhofer:
Gottfried Mayerhofer - Predicile domnului Iisus Hristos 0.99 '{Spiritualitate}.docx
Gottfried Mayerhofer - Secretele vietii 0.99 '{Spiritualitate}.docx

./Grace Goodwin:
Grace Goodwin - Aproape perfect 0.99 '{Dragoste}.docx
Grace Goodwin - Barbatul primei iubiri 0.99 '{Dragoste}.docx
Grace Goodwin - Fiul tatalui vitreg 0.99 '{Dragoste}.docx

./Grace Livingston Hill:
Grace Livingston Hill - Fata din padure 0.99 '{Dragoste}.docx

./Grahame Keneth:
Grahame Keneth - Vantul prin salcii 5.0 '{Copii}.docx

./Graham Greene:
Graham Greene - Agentul secret 0.6 '{ActiuneComando}.docx
Graham Greene - Americanul linistit 0.9 '{ActiuneComando}.docx
Graham Greene - Miezul lucrurilor 1.0 '{Thriller}.docx

./Graham Hancock:
Graham Hancock - Amprentele zeilor 0.7 '{MistersiStiinta}.docx
Graham Hancock - Enciclopedia civilizatiilor 1.0 '{MistersiStiinta}.docx

./Graham Joyce:
Graham Joyce - Un fel de basm 1.0 '{Tineret}.docx

./Graham Masterton:
Graham Masterton - Djin 1.0 '{Horror}.docx
Graham Masterton - Manitou 2.0 '{Horror}.docx

./Graham Swift:
Graham Swift - Lumina zilei 0.7 '{Literatura}.docx
Graham Swift - Maine 1.0 '{Literatura}.docx
Graham Swift - Ultima comanda 0.99 '{Literatura}.docx

./Grazia Deledda:
Grazia Deledda - Iedera 1.0 '{Dragoste}.docx

./Greer Hendricks:
Greer Hendricks - Fata anonima 1.0 '{Literatura}.docx
Greer Hendricks - Sotia dintre noi 1.0 '{Diverse}.docx

./Greg Bear:
Greg Bear - Calea - V1 Eon 1.0 '{SF}.docx
Greg Bear - Calea - V2 Eternitate 1.0 '{SF}.docx
Greg Bear - Muzica sangelui 0.99 '{SF}.docx
Greg Bear - Radioul lui Darwin 2.0 '{SF}.docx

./Greg Egan:
Greg Egan - Calarind crocodilul 1.0 '{SF}.docx
Greg Egan - Carantina 1.0 '{SF}.docx
Greg Egan - Caseta de valori 0.9 '{SF}.docx
Greg Egan - Distres 2.0 '{SF}.docx
Greg Egan - Luminescent 2.0 '{SF}.docx
Greg Egan - Santul fortaretei 0.99 '{SF}.docx
Greg Egan - Scara lui Schild 1.0 '{SF}.docx
Greg Egan - Surori de sange 0.99 '{SF}.docx

./Gregg Braden:
Gregg Braden - Efectul Isaia 0.9 '{Spiritualitate}.docx

./Gregg Chamberlain:
Gregg Chamberlain - Elixirul dragostei 1.0 '{SF}.docx

./Gregor von Rezzori:
Gregor von Rezzori - Memoriile unui antisemit 0.99 '{Istorie}.docx

./Gregory Benford:
Gregory Benford - Artifact 1.0 '{SF}.docx
Gregory Benford - Manassas, din nou 0.99 '{SF}.docx
Gregory Benford - Poveste de razboi 0.99 '{SF}.docx
Gregory Benford - Timperfect 1.0 '{CalatorieinTimp}.docx
Gregory Benford - Viitorul sistemului Jupiterian 0.99 '{SF}.docx

./Gregory Benford & David Brin:
Gregory Benford & David Brin - Inima cometei 1.0 '{SF}.docx

./Gregory David Roberts:
Gregory David Roberts - Shantaram 2.0 '{Literatura}.docx

./Gregory Katsoulis:
Gregory Katsoulis - Word$ - V1 Toate drepturile rezervate 1.0 '{Literatura}.docx

./Gregory Mcdonald:
Gregory Mcdonald - Pe muchie de cutit 1.0 '{Politista}.docx

./Gregory Samak:
Gregory Samak - Cartea secreta 1.0 '{Literatura}.docx

./Greg Walker:
Greg Walker - Stiletul 1.0 '{ActiuneRazboi}.docx
Greg Walker - Urmarire in Alaska 1.0 '{ActiuneRazboi}.docx

./Greig Beck:
Greig Beck - Fosila 1.0 '{SF}.docx

./Grid Modorcea:
Grid Modorcea - Baietii de bani gata 2.0 '{Literatura}.docx

./Grigore Alexandrescu:
Grigore Alexandrescu - Magarul rasfatat 1.0 '{Versuri}.docx
Grigore Alexandrescu - Poezii si proza 1.0 '{Versuri}.docx

./Grigore Bajenaru:
Grigore Bajenaru - Banul Maracine 5.0 '{IstoricaRo}.docx
Grigore Bajenaru - Buna dimineata baieti 0.8 '{Tineret}.docx
Grigore Bajenaru - Cismigiu & Comp 1.0 '{Tineret}.docx
Grigore Bajenaru - Inelul lui Dragos Voda 5.0 '{ClubulTemerarilor}.docx
Grigore Bajenaru - Taina lui Mircea Voievod 5.0 '{IstoricaRo}.docx

./Grigore Beuran:
Grigore Beuran - Cifrul Petre Petrescu 1.0 '{Politista}.docx

./Grigore Botezatu:
Grigore Botezatu - Basme populare romanesti V1 0.9 '{BasmesiPovesti}.docx

./Grigore Caraza:
Grigore Caraza - Aiudul insangerat 0.9 '{Comunism}.docx

./Grigore de Nyssa:
Grigore de Nyssa - Despre suflet si inviere 1.0 '{Spiritualitate}.docx

./Grigore Dumitrescu:
Grigore Dumitrescu - Demascarea 0.99 '{Literatura}.docx

./Grigore Ureche:
Grigore Ureche - Letopisetul tarii Moldovei 1.0 '{ClasicRo}.docx

./Grigorian Bivolaru:
Grigorian Bivolaru - Curs Yoga an 01 0.7 '{Spiritualitate}.docx
Grigorian Bivolaru - Curs Yoga an 02 0.7 '{Spiritualitate}.docx
Grigorian Bivolaru - Curs Yoga an 03 0.7 '{Spiritualitate}.docx
Grigorian Bivolaru - Curs Yoga an 04 0.7 '{Spiritualitate}.docx
Grigorian Bivolaru - Curs Yoga an 05 0.7 '{Spiritualitate}.docx
Grigorian Bivolaru - Curs Yoga an 06 0.7 '{Spiritualitate}.docx
Grigorian Bivolaru - Curs Yoga an 07 0.7 '{Spiritualitate}.docx
Grigorian Bivolaru - Curs Yoga an 08 0.7 '{Spiritualitate}.docx
Grigorian Bivolaru - Curs Yoga an 10 0.7 '{Spiritualitate}.docx
Grigorian Bivolaru - Curs Yoga an 12 0.7 '{Spiritualitate}.docx
Grigorian Bivolaru - Curs Yoga an 13 0.7 '{Spiritualitate}.docx
Grigorian Bivolaru - Curs Yoga an 14 0.7 '{Spiritualitate}.docx
Grigorian Bivolaru - Curs Yoga an 15 0.7 '{Spiritualitate}.docx

./Grigorie Gh. Comsa:
Grigorie Gh. Comsa - Baptismul in Romania 0.8 '{Religie}.docx

./Grigorie Palama:
Grigorie Palama - Cuvant intai 0.9 '{Religie}.docx

./Grigori Kapita:
Grigori Kapita - V1 Atlasul bioenergetic al omului 0.6 '{Spiritualitate}.docx
Grigori Kapita - V2 Vampirii energetici 0.7 '{Spiritualitate}.docx
Grigori Kapita - V3 Anatomia schimbului bioenergetic 0.6 '{Spiritualitate}.docx
Grigori Kapita - V4 Apararea familiei de atacurile energetice 0.7 '{Spiritualitate}.docx
Grigori Kapita - V5 Atacurile energetice 0.7 '{Spiritualitate}.docx
Grigori Kapita - V6 Bioenergetica locuintei 0.7 '{Spiritualitate}.docx

./Guglielmo Cavallo:
Guglielmo Cavallo - Omul bizantin 0.8 '{Diverse}.docx

./Guillaume Apollinaire:
Guillaume Apollinaire - Amorurile unui hospodar 0.7 '{Diverse}.docx
Guillaume Apollinaire - Amorurile unui print 1.0 '{Erotic}.docx
Guillaume Apollinaire - Roma familiei Borgia 0.8 '{AventuraIstorica}.docx

./Guillaume Musso:
Guillaume Musso - Ce-as fi eu fara tine 1.0 '{Thriller}.docx
Guillaume Musso - Central Park 1.0 '{Thriller}.docx
Guillaume Musso - Chemarea ingerului 2.0 '{Thriller}.docx
Guillaume Musso - Dupa sapte ani 1.0 '{Thriller}.docx
Guillaume Musso - Fata de hartie 1.0 '{Thriller}.docx
Guillaume Musso - Fata din Brooklyn 1.0 '{Thriller}.docx
Guillaume Musso - Fata si noaptea 1.0 '{Thriller}.docx
Guillaume Musso - Maine 1.0 '{Thriller}.docx
Guillaume Musso - Salveaza-ma 2.0 '{Thriller}.docx
Guillaume Musso - Si dupa 2.0 '{Thriller}.docx
Guillaume Musso - Un apartament la Paris 1.0 '{Thriller}.docx
Guillaume Musso - Vei fi acolo 1.0 '{Thriller}.docx

./Guillaume Prevost:
Guillaume Prevost - Asasinul si profetul 1.0 '{Thriller}.docx
Guillaume Prevost - Piatra sculptata 1.0 '{AventuraTineret}.docx

./Guillermo Del Toro & Chuck Hogan:
Guillermo Del Toro & Chuck Hogan - Molima - V1 Molima 2.0 '{SF}.docx

./Guillermo Infante:
Guillermo Infante - Nimfa nestatornica 0.99 '{Literatura}.docx

./Guillermo Martinez:
Guillermo Martinez - Crimele din Oxford 0.8 '{Politista}.docx

./Gunter Grass:
Gunter Grass - Anestezie locala 0.9 '{Literatura}.docx
Gunter Grass - Aparatul de fotografiat 0.99 '{Literatura}.docx
Gunter Grass - Decojind ceapa 0.99 '{Literatura}.docx
Gunter Grass - In mers de rac 1.0 '{Literatura}.docx
Gunter Grass - Pisica si soarecele 0.99 '{Literatura}.docx
Gunter Grass - Toba de tinichea 0.8 '{Literatura}.docx

./Gunther Krupkat:
Gunther Krupkat - Nabou 1.0 '{SF}.docx

./Gustave Flaubert:
Gustave Flaubert - Corespondenta 0.8 '{ClasicSt}.docx
Gustave Flaubert - Doamna Bovary 0.9 '{ClasicSt}.docx
Gustave Flaubert - Educatia sentimentala 1.0 '{ClasicSt}.docx
Gustave Flaubert - Ispitirea sfantului Anton 0.9 '{ClasicSt}.docx
Gustave Flaubert - Salammbo 1.0 '{Dragoste}.docx
Gustave Flaubert - Trei povestiri 0.99 '{ClasicSt}.docx

./Gustav Meyrink:
Gustav Meyrink - Opalul 0.99 '{Literatura}.docx
Gustav Meyrink - Secretul castelului Hathaway 0.9 '{Literatura}.docx

./Gustavo Adolfo Becquer:
Gustavo Adolfo Becquer - Muntele strigoilor 0.99 '{Literatura}.docx

./Gustavo Adolfo Loria Rivel:
Gustavo Adolfo Loria Rivel - Evanghelia dupa Toma 0.7 '{Spiritualitate}.docx

./Gustavo Dessal:
Gustavo Dessal - Operatiunea Afrodita 0.99 '{Literatura}.docx

./Guy de Cars:
Guy de Cars - Bruta 2.0 '{Thriller}.docx

./Guy de Maupassant:
Guy de Maupassant - Bel ami 2.0 '{ClasicSt}.docx
Guy de Maupassant - Horla 0.99 '{ClasicSt}.docx
Guy de Maupassant - Inima noastra 1.0 '{ClasicSt}.docx
Guy de Maupassant - Mai tare ca moartea 1.0 '{ClasicSt}.docx
Guy de Maupassant - Mont Oriol 1.0 '{ClasicSt}.docx
Guy de Maupassant - O viata 1.0 '{ClasicSt}.docx
Guy de Maupassant - Pierre si Jean 1.0 '{ClasicSt}.docx
Guy de Maupassant - Vagabondul 1.0 '{ClasicSt}.docx

./Guy Kay:
Guy Kay - Pamantul iubit de zei 1.0 '{Literatura}.docx

./Guy le Luhandre:
Guy le Luhandre - Flacari sub ocean 2.0 '{ActiuneRazboi}.docx

./Guy Lespig:
Guy Lespig - Ultima rafala in Birmania 1.0 '{ActiuneRazboi}.docx

./Guy Rachet:
Guy Rachet - Romanul Piramidelor - V1 Keops si conjuratia ibisului 1.0 '{AventuraIstorica}.docx
Guy Rachet - Romanul Piramidelor - V2 Keops si marea piramida 1.0 '{AventuraIstorica}.docx
Guy Rachet - Romanul Piramidelor - V3 Kefren si Didufri 1.0 '{AventuraIstorica}.docx

./Guy Vander:
Guy Vander - Razbunarea corsarului 2.0 '{Western}.docx

./Guzel Iahina:
Guzel Iahina - Zuleiha deschide ochii 1.0 '{Literatura}.docx

./Gwendoline Tavare:
Gwendoline Tavare - Mireasa marii 1.0 '{Dragoste}.docx

./Gyles Brandreth:
Gyles Brandreth - Oscar Wilde si crimele la lumina lumanarii 1.0 '{Politista}.docx

./Gyorgy Gyorfi Deak:
Gyorgy Gyorfi Deak - Afacerea Adam 0.99 '{SF}.docx
Gyorgy Gyorfi Deak - Doua palose 0.99 '{SF}.docx
Gyorgy Gyorfi Deak - Dreptul la succes 0.99 '{SF}.docx
Gyorgy Gyorfi Deak - Fabula 0.99 '{SF}.docx
Gyorgy Gyorfi Deak - Livada 0.99 '{SF}.docx
Gyorgy Gyorfi Deak - Marea si eterna dragoste 1.0 '{SF}.docx
Gyorgy Gyorfi Deak - Missa Solemnis 0.99 '{SF}.docx
Gyorgy Gyorfi Deak - Naravul din fire 0.99 '{SF}.docx
Gyorgy Gyorfi Deak - O dorinta implinita 0.9 '{SF}.docx
Gyorgy Gyorfi Deak - Parul auriu 0.99 '{SF}.docx
Gyorgy Gyorfi Deak - Umbre pe marginea falezei 0.99 '{SF}.docx
Gyorgy Gyorfi Deak - Un mar de moda veche 0.99 '{SF}.docx
Gyorgy Gyorfi Deak - Un soare ca un ochi de sticla 0.99 '{SF}.docx

./Gyorgy Kulin:
Gyorgy Kulin - Mesajul celei de-a opta planete 0.99 '{SF}.docx

./Gytha Lodge:
Gytha Lodge - DCI Jonah Sheens - V1 Pierduta in asteptare 1.0 '{Literatura}.docx
```

